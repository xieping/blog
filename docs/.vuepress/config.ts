module.exports = {
  base: '/blog/', // 部署地址文件夹 项目名
  title: 'blog title', // 网站的title
  description: '谢萍的博客 记录学习工作生活', // 描述
  head: [
    ['link', { rel: 'icon', href: '/logo.png' }], // 网站logo
  ],
  dest: '/dist', // 打包目录
  plugins: ['@vuepress/back-to-top', '@vuepress/blog']
}